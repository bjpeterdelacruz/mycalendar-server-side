var express = require("express");
var path = require("path");
var bodyParser = require("body-parser");
var mongodb = require("mongodb");
var ObjectID = mongodb.ObjectID;

var EVENTS_COLLECTION = "events";
var HOLIDAYS_COLLECTION = "holidays";
var CONVERSIONS_COLLECTION = "conversions";

var app = express();
app.use(express.static(__dirname + "/public"));
app.use(bodyParser.json());

// Create a database variable outside of the database connection callback to reuse the connection pool in your app.
var db;

// Connect to the database before starting the application server.
mongodb.MongoClient.connect(process.env.MONGODB_URI, function (err, database) {
  if (err) {
    console.log(err);
    process.exit(1);
  }

  // Save database object from the callback for reuse.
  db = database;
  console.log("Database connection ready");

  // Initialize the app.
  var server = app.listen(process.env.PORT || 8080, function () {
    var port = server.address().port;
    console.log("App now running on port", port);
  });
});

// Generic error handler used by all endpoints.
function handleError(res, reason, message, code) {
  console.log("ERROR: " + reason);
  res.status(code || 500).json({"error": message});
}

// See: http://stackoverflow.com/questions/32500073/request-header-field-access-control-allow-headers-is-not-allowed-by-itself-in-pr

app.all('*', function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'DELETE, GET, OPTIONS, POST, PUT');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  if ('OPTIONS' == req.method) {
      res.sendStatus(200);
  } else {
      next();
  }
});

app.post("/events", function(req, res) {
  var newEvents = req.body;
  newEvents.updated = new Date();

  if (!(newEvents.deviceID) || !(newEvents.name)) {
    handleError(res, "Invalid user input", "Must provide device ID and name for events.", 400);
    return;
  }

  db.collection(EVENTS_COLLECTION).findOne({deviceID: req.params.id, name: req.params.name}, function(err, result) {
    if (err) {
      handleError(res, err.message, "Failed to get events.");
    } else {
      if (result) {
        db.collection(EVENTS_COLLECTION).deleteOne({deviceID: newEvents.deviceID, name: newEvents.name}, function(err, result) {
          if (err) {
            handleError(res, err.message, "Failed to update events.");
          } else {
            insert(res, db, newEvents);
          }
        });
      } else {
        insert(res, db, newEvents);
      }
    }
  });
});

function insert(res, db, newEvents) {
  console.log(newEvents);
  db.collection(EVENTS_COLLECTION).insertOne(newEvents, function(err, result) {
    if (err) {
      handleError(res, err.message, "Failed to save events.");
    } else {
      res.status(201).json(result.ops[0]);
    }
  });
}

app.get("/events/:id", function(req, res) {
  db.collection(EVENTS_COLLECTION).find({deviceID: req.params.id}).toArray(function(err, result) {
    if (err) {
      handleError(res, err.message, "Failed to get events.");
    } else {
      res.status(200).json(result);
    }
  });
});

app.get("/events/:id/:name", function(req, res) {
  db.collection(EVENTS_COLLECTION).findOne({deviceID: req.params.id, name: req.params.name}, function(err, result) {
    if (err) {
      handleError(res, err.message, "Failed to get events.");
    } else {
      res.status(200).json(result.events);
    }
  });
});

app.delete("/events/:id/:name", function(req, res) {
  db.collection(EVENTS_COLLECTION).deleteOne({deviceID: req.params.id, name: req.params.name}, function(err, result) {
    if (err) {
      handleError(res, err.message, "Failed to delete events.");
    } else {
      res.status(204).end();
    }
  });
});

app.get("/holidays", function(req, res) {
  db.collection(HOLIDAYS_COLLECTION).find({}).toArray(function(err, result) {
    if (err) {
      handleError(res, err.message, "Failed to get holidays.");
    } else {
      res.status(200).json(result[0].holidays);
    }
  });
});

app.post("/conversions", function(req, res) {
  var conversions = req.body;
  conversions.updated = new Date();

  if (!(conversions.deviceID)) {
    handleError(res, "Invalid input", "Must provide device ID.", 400);
    return;
  }

  db.collection(CONVERSIONS_COLLECTION).insertOne(conversions, function(err, result) {
    if (err) {
      handleError(res, err.message, "Failed to create conversions.");
    } else {
      res.status(201).json(result.ops[0]);
    }
  });
});

app.get("/conversions/:id", function(req, res) {
  db.collection(CONVERSIONS_COLLECTION).findOne({deviceID: req.params.id}, function(err, result) {
    if (err) {
      handleError(res, err.message, "Failed to get conversions.");
    } else {
      res.status(200).json(result ? result.conversions : {});
    }
  });
});

app.put("/conversions/:id", function(req, res) {
  db.collection(CONVERSIONS_COLLECTION).findOne({deviceID: req.params.id}, function(err, result) {
    if (err) {
      handleError(res, err.message, "Failed to get conversions.");
    } else {
      console.log(req.body);
      if (result) {
        result.conversions.push(req.body.conversions[0]);
        db.collection(CONVERSIONS_COLLECTION).update({deviceID: req.params.id}, {$set: {conversions: result.conversions}}, function(err, result) {
          if (err) {
            handleError(res, err.message, "Failed to update conversions.");
          } else {
            res.status(200).json({"success":1});
          }
        });
      } else {
        db.collection(CONVERSIONS_COLLECTION).insertOne(req.body, function(err, result) {
          if (err) {
            handleError(res, err.message, "Failed to create conversions.");
          } else {
            res.status(200).json({"success":1});
          }
        });
      }
    }
  });
});

// See: http://stackoverflow.com/questions/30497413/angular-http-delete-cors-error-preflight-request

/*
app.options("/conversions/:id", function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'DELETE');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With,Content-Type');

    next();
});
*/

app.delete("/conversions/:id", function(req, res) {
  db.collection(CONVERSIONS_COLLECTION).remove({deviceID: req.params.id}, function(err, result) {
    if (err) {
      handleError(res, err.message, "Failed to delete conversions.");
    } else {
      res.status(200).json({"success":1});
    }
  });
});
