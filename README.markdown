# myCalendar

## Server Side Implementation

### About

This repository contains the server side (backend) implementation of **myCalendar**.

The server was developed using [Node.js](https://nodejs.org/en).

The server uses [MongoDB](http://www.mongodb.com) to store data.

### Links

A public web version of **myCalendar** can be found [here](http://www.bjpeterdelacruz.com/calendar/index.html).
